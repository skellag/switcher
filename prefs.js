let sampleTableData = [{
  "TableName": "SAP Hybris",
  "PeopleAtATableArray": [{
    "id": 1,
    "ServerName": "Author",
    "ServerURL": "https://google.com"
  }, {
    "id": 2,
    "ServerName": "Publish",
    "ServerURL": "https://google.com"
  }, {
    "id": 3,
    "ServerName": "Dispatcher",
    "ServerURL": "https://google.com"
  }]
}, {
  "TableName": "WooCommerce",
  "PeopleAtATableArray": [{
    "ServerName": "Author",
    "ServerURL": "https://google.com"
  }, {
    "ServerName": "Publish",
    "ServerURL": "https://google.com"
  }, {
    "ServerName": "Dispatcher",
    "ServerURL": "https://google.com"
  }]
}, {
  "TableName": "Salesforce Commerce Cloud",
  "PeopleAtATableArray": [{
    "ServerName": "Author",
    "ServerURL": "https://google.com"
  }, {
    "ServerName": "Publish",
    "ServerURL": "https://google.com"
  }]
}];

let emptyServer = {
  "ServerName": "",
  "ServerURL": ""
}

function addGlobalServer() {
  var newServer = document.getElementById('newGlobalServer').value;
  if (!newServer) {
    return ;
  }
  sampleTableData.push({
    TableName: newServer,
    PeopleAtATableArray: []
  });
  init();
  document.getElementById('newGlobalServer').value = '';
}

( function ( $ ) {
  $.fn.renderTree = function ( data, dataOptions, renderOptions ) {
    let dataSettings = $.extend( {
      parentTitle: "title",
      parentArray: "children",
      childArray: "children",
      grandchildColumns: []
    }, dataOptions );

    let renderSettings = $.extend( {
      parentContainerClass: "parent-container",
      parentContainerHeadingClass: "parent-container-heading",
      parentTitleClass: "parent-title",
      parentBodyClass: "parent-body",
      childContainerClass: "child-container",
      childListClass: "child-list",
      childListItemClass: "child-list-item"
    }, renderOptions );

    $( this ).empty();
    let treeData = "";

    data.forEach( function ( parent, index, array ) {
      treeData += '<div class="' + renderSettings.parentContainerClass + ' panel panel-default" id="parent-' + index + '">';
      treeData += '<div class="' + renderSettings.parentContainerHeadingClass + ' panel-heading"><h4 class="panel-title">';
      treeData += '<a class="' + renderSettings.parentTitleClass + '" data-toggle="collapse" aria-expanded="true" data-parent="#parent-' + index + '"  href="#parent-collapse-' + index + '">';
      treeData += parent[ dataSettings.parentTitle ];
      treeData += '</a></h4>';
      treeData += '<div><button type="button" onclick=addItem(this) class="btn btn-success server-btn" id="'+ parent[ dataSettings.parentTitle ] +'">Add</button>';
      treeData += '<button type="button" onclick=removeGlobal(this) class="btn btn-danger server-btn" id="'+ parent[ dataSettings.parentTitle ] +'">Remove</button></div></div>';
      treeData += '<div id="parent-collapse-' + index + '" class="panel-show collapse in" aria-expanded="true"><div class="' + renderSettings.parentBodyClass + ' panel-body">';
      parent[ dataSettings.parentArray ].forEach( function ( child, index2, array ) {
        treeData += '<div class="' + renderSettings.childContainerClass + ' section">';
        treeData += '<div id="child-' + index2 + '"><ul class="' + renderSettings.childListClass + '">';
        treeData += '<li data-selected="false" class="' + renderSettings.childListItemClass + '"><div class="row item">';
        dataSettings.grandChildColumns.forEach( function ( grandchild ) {
          treeData += '<input type="text" value="' + child[ grandchild.title ] + '"' + 'class="form-control ' + grandchild.column_size + '" onkeyup=modifyItem(this)>';
        } );
        treeData += '<button type="button" id="'+ $(parent)[0].TableName + '&&' + child.ServerName +'" class="btn btn-warning btn-circle" onclick=removeItem(this)><i class="glyphicon glyphicon-remove"></i></button>';
        treeData += '</div></li>';
        treeData += '</ul></div></div>';
      } );
      treeData += '</div></div></div>'
    } );

    $( this ).html( treeData );
  };

}( jQuery ) );

function removeGlobal(server) {
  sampleTableData = sampleTableData.filter(item => item.TableName !== $(server)[0].id);
  init();
}

function addItem(element) {
  sampleTableData = sampleTableData.map(item => {
    if (item.TableName === $(element)[0].id) {
      item.PeopleAtATableArray.push(emptyServer);
    }
    return item;
  });
  init();
}

function modifyItem(element) {
  var tableName = $(element).closest('.parent-container').find('.parent-title')[0].text;
  console.log($(element));
  var type = $(element)[0].classList[1];
  var defaultValue = $(element)[0].defaultValue;
  console.log(defaultValue);
  var updatedValue = $(element)[0].value;
  sampleTableData.map(global => {
    if (global.TableName === tableName) {
      global.PeopleAtATableArray = global.PeopleAtATableArray.map(local => {
        if (type === 'title' && local.ServerName === defaultValue) {
          local.ServerName = updatedValue;
          $(element)[0].defaultValue = updatedValue;
        }
        if (type === 'url' && local.ServerURL === defaultValue) {

        }
        return local;
      });
    }
    return global;
  });
}

function removeItem(item) {
  var server = $(item)[0].id.split('&&');
  sampleTableData = sampleTableData.map(global => {
    if (global.TableName == server[0]) {
      global.PeopleAtATableArray = global.PeopleAtATableArray.filter(local => local.ServerName !== server[1]);
    }
    return global;
  });
  init();
}

function init() {
  $('#treeContainer').renderTree(sampleTableData, {
    parentTitle: "TableName",
    parentArray: "PeopleAtATableArray",
    grandChildColumns: [{
      title: "ServerName",
      column_size: "title"
    }, {
      title: "ServerURL",
      column_size: "url"
    }]
  }, {});
}

init();