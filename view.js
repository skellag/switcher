function server(params) {
  this.type = params.type;
  this.alias = params.alias;
  this.authordomain = params.authordomain;
  this.publishdomain = params.publishdomain

  this.getAlias = function() {
    return this.alias;
  };

  this.getAuthDomain = function() {
    return this.authordomain;
  };

  this.getPublDomain = function() {
    return this.publishdomain;
  };

  this.render = function() {
    var to = document.createElement("table");
    var alitr = document.createElement("tr");
    $(alitr).addClass("alias");
    var tr = document.createElement("tr");
    to.appendChild(alitr);
    to.appendChild(tr);

    var ali = document.createElement("td");
    $(ali).attr("colspan", 2);
    alitr.appendChild(ali);
    var name = document.createTextNode(this.getAlias());
    ali.appendChild(name);

    var tdauth = document.createElement("td");
    var authVal = document.createTextNode(this.getAuthDomain());
    tdauth.appendChild(authVal);
    $(tdauth).addClass("author");
    $(tdauth).attr("author-domain", this.getAuthDomain());
    $(tdauth).attr("width", "50%");
    $(tdauth).click(function(evt) {
      openLink($(evt.target).attr("author-domain"));
    });

    var tdpubl = document.createElement("td");
    var publVal = document.createTextNode(this.getPublDomain());
    tdpubl.appendChild(publVal);
    $(tdpubl).addClass("publish");
    $(tdpubl).attr("publish-domain", this.getPublDomain());
    $(tdpubl).attr("width", "50%");
    $(tdpubl).click(function(evt) {
      openLink($(evt.target).attr("publish-domain"));
    });

    tr.appendChild(tdauth);
    tr.appendChild(tdpubl);

    $(to).addClass("server");
    $("#servers").append(to);
  };
};

function create(settings) {
  for (var i = 0; i < settings.domains.length; i++) {
    settings.domains[i] = new server(settings.domains[i]);
    settings.domains[i].render();
  }
}

$(document).ready(function() {
  createPopup();
});