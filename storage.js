function savePrefs() {
  var servers = $("#servers");

  var settings = {
    domains : []
  };
  var envBlock = [];
  var servers = $(servers).find(".server");

  for (var j = 0; j < servers.length; j++) {
    var server = servers[j];
    var servObj = {
      alias : "",
      authordomain : "",
      publishdomain : "",
    };

    servObj.alias = $(server).find("[name=server-alias]").val();
    servObj.authordomain = $(server).find("[name=author-domain]").val();
    servObj.publishdomain = $(server).find("[name=publish-domain]").val();
    settings.domains.push(servObj);
  }

  chrome.storage.sync.set({
    'settings' : settings
  }, function() {
    $("#status").fadeIn();
    setTimeout(function() {
      $("#status").fadeOut();
    }, 1000);
  });
}

function loadOptions(otherSettings) {
  if (otherSettings) {
    if (otherSettings && otherSettings.domains) {
      settings = otherSettings;
      $("#servers").empty();
      for (var i = 0; i < otherSettings.domains.length; i++) {
        $("#servers").append(createServer(otherSettings.domains[i]));
      }
    }
  } else {
    chrome.storage.sync.get('settings', function(items) {
      if (items && items.settings && items.settings.domains) {
        settings = items.settings;
        for (var i = 0; i < items.settings.domains.length; i++) {
          $("#servers").append(createServer(items.settings.domains[i]));
        }
      } else {
        reset();
      }
    });
  }
}

function createPopup() {
  chrome.storage.sync.get('settings', function(items) {
    if (items.settings == null) {
      settings = defaultSettings;
      create(settings);
      storage.set({
        'settings' : settings
      }, function() {
        if (chrome.runtime.lastError != null) {
          console.log("error" + chrome.runtime.lastError);
        }
      });
    } else {
      settings = items.settings;
      create(settings);
    }
  });
}

function reset() {
  settings = defaultSettings;
  loadOptions(settings);
}

var getUri = function(href) {
  var l = document.createElement("a");
  l.href = href;
  return l;
};

function openLink(domain) {
  chrome.tabs.query({
    active : true,
    currentWindow : true
  }, function(tabs) {
    var currentUrl = tabs[0].url;
    var href = getUri(currentUrl)

    if (currentUrl.indexOf('cf#/') > -1) {
      // from authoring to publish
      currentUrl = currentUrl.replace('cf#/', '')
    }

    window.open(domain + getUri(currentUrl).pathname, '_blank');
    window.focus();

  });
}

var settings;

var defaultSettings = {
  domains : [ {
    alias : "dev",
    authordomain : "http://localhost:4502/",
    publishdomain : "http://localhost:4503/",
  }, {
    alias : "qa",
    authordomain : "http://localhost:4502/",
    publishdomain : "http://localhost:4503/",
  } ]
}